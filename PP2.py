# coding=utf-8
import time
import wiringpi

wiringpi.wiringPiSetupGpio()
wiringpi.pinMode(18, wiringpi.GPIO.PWM_OUTPUT)
wiringpi.pinMode(19, wiringpi.GPIO.PWM_OUTPUT)
wiringpi.pwmSetMode(wiringpi.GPIO.PWM_MODE_MS)
wiringpi.pwmSetClock(192)
wiringpi.pwmSetRange(2000)

counter = False


def Search(list):
    global obiekt
    global akcja
    global miara
    global NaczyO
    global counter

    for index in range(len(list)):
        listLower = list[index].lower()
        if listLower in obiekty[0]:
            obiekt = "skrzynia"
        if listLower in obiekty[1]:
            obiekt = "beczka"
        if listLower in polecenia[0]:
            akcja = "góra"
        if listLower in polecenia[1]:
            akcja = "obrót"
        if listLower in polecenia[2]:
            akcja = "dół"
        if listLower in polecenia[3]:
            akcja = "stop"
        if listLower in polecenia[4]:
            akcja = "start"
        if listLower in polecenia[5]:
            akcja = "lewo"
        if listLower in polecenia[6]:
            akcja = "prawo"
        if listLower in polecenia[7]:
            NaczyO = "na"
        if listLower in polecenia[8]:
            NaczyO = "o"
        if listLower in polecenia[9]:
            akcja = "rozwin"
        if listLower in polecenia[10]:
            akcja = "zwin"
        if listLower in polecenia[11]:
            akcja = "cc"
        if listLower in polecenia[12]:
            akcja = "herbata"
        if listLower in miary[0]:
            miara = "stopnie"
        if listLower in miary[1]:
            miara = "metry"


def Komenda(akcja, obiekt, miara, NaczyO):
    komenda = []
    global counter
    if akcja == "start":
        komenda.append("Dźwig rusza")
    if akcja == "stop":
        komenda.append("Dźwig się zatrzymał")
        wiringpi.pwmWrite(18, 153)
        wiringpi.pwmWrite(19, 0)
    if akcja == "lewo":
        komenda.append("Dźwig porusza się w lewo")
    if akcja == "prawo":
        komenda.append("Dźwig porusza się w prawo")
    if akcja == "herbata":
        print("Przygotowuję Herbatę")
        wiringpi.pwmWrite(18,3)
        time.sleep(2)
        wiringpi.pwmWrite(18, 0)
        time.sleep(0.5)
        wiringpi.pwmWrite(19, 150)
        time.sleep(1)
        wiringpi.pwmWrite(19, 153)
        time.sleep(0.5)
        wiringpi.pwmWrite(18, 1750)
        time.sleep(4.90)
        wiringpi.pwmWrite(18, 0)
        time.sleep(10)
        wiringpi.pwmWrite(18,3)
        time.sleep(2)
        wiringpi.pwmWrite(18,0)
        time.sleep(1)
        wiringpi.pwmWrite(19, 180)
        time.sleep(0.2)
        wiringpi.pwmWrite(19,160)
        time.sleep(0.3)
        wiringpi.pwmWrite(19,153)
        time.sleep(0.5)
        wiringpi.pwmWrite(18, 1750)
        time.sleep(6.40)
        wiringpi.pwmWrite(18, 0)
    if akcja == "góra" and counter== False:
        komenda.append("Dźwig się unosi")
        wiringpi.pwmWrite(19, 150)
        time.sleep(1)
        wiringpi.pwmWrite(19, 153)
        counter = not counter
    if akcja == "dół" and counter == True:
        komenda.append("Dźwig się obniża")
        wiringpi.pwmWrite(19, 160)
        time.sleep(1)
        wiringpi.pwmWrite(19, 153)
        counter = not counter
    if akcja == "zwin":
        wiringpi.pwmWrite(18, 3)
        time.sleep(2)
        wiringpi.pwmWrite(18, 0)
    if akcja == "rozwin":
        wiringpi.pwmWrite(18, 1750)
        time.sleep(2)
        wiringpi.pwmWrite(18, 0)
    if akcja == "stopnie":
        komenda.append("Dźwig porusza się o ilość stopni")
    if akcja == "cc":
        counter = not counter
        #komenda.append(cc)
    if akcja == "obrót":
        komenda.append("Dźwig się obraca")
    if obiekt == "skrzynia":
        komenda.append("Dźwig szuka skrzyni")
    if obiekt == "beczka":
        komenda.append("Dźwig szuka beczki")
    if miara == "metry":
        komenda.append("metrów")
    if NaczyO == "o":
        komenda.append("o")
    if NaczyO == "na":
        komenda.append("na")

    for i in range(len(komenda)):
        print(komenda[i])


obiekty = [["skrzynia", "skrzynię", "skrzynkę", "skrzynka"], ["beczka", "beczkę", "beczółkę", "beczółka"]]
polecenia = [["podnieś", "unieś", "wznieś", "góra", "gora"], ["obrót", "obkręć"], ["opuść", "obniż", "dół", "dol"],
             ["zatrzymaj", "stop"], ["start", "rozpocznij"], ["lewo"], ["prawo"], ["na"], ["o"], ["rozwiń", "rozwin"],
             ["zwiń", "zwin"], ["cc"], ["herbata", "herbate", "herbatke", "herbatę", "herbatkę"]]
miary = [["stopnie", "stopni", "st"], ["metry", "metrów"]]

# main

while True:
    obiekt = ""
    akcja = ""
    miara = ""
    NaczyO = ""
    print(counter)
    myString = input("Podaj komende!")
    list = myString.split(" ")
    listLower = list[0].lower()
    if len(list) == 1 and listLower == "koniec":
        print("koniec")
        break
    Search(list)
    Komenda(akcja, obiekt, miara, NaczyO)

