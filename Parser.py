# coding=utf-8

#import RPi.GPIO as GPIO
import time

#GPIO.setmode(GPIO.BCN)
#GPIO.setup(18, GPIO.OUT)
#p = GPIO.PWN(18, 50)
#p.start(7.29999999999999999)
counter = 0


def Search(list):
    global obiekt
    global akcja
    global miara
    global NaczyO
    global counter

    for index in range(len(list)):
        listLower = list[index].lower()
        if listLower in obiekty[0]:
            # print ("skrzynia")
            obiekt = "skrzynia"
        if listLower in obiekty[1]:
            # print ("beczka")
            obiekt = "beczka"
        if listLower in polecenia[0]:
            # print ("podnieś")
            akcja = "góra"
        if listLower in polecenia[1]:
            # print ("obrót")
            akcja = "obrót"
        if listLower in polecenia[2]:
            # print ("dół")
            akcja = "dół"
        if listLower in polecenia[3]:
            akcja = "stop"
        if listLower in polecenia[4]:
            akcja = "start"
        if listLower in polecenia[5]:
            # print ("zatrzymaj")
            akcja = "lewo"
        if listLower in polecenia[6]:
            # print ("lewo")
            akcja = "prawo"
        if listLower in polecenia[7]:
            NaczyO = "na"
        if listLower in polecenia[8]:
            NaczyO = "o"
        if listLower in miary[0]:
            # print ("prawo")
            miara = "stopnie"
        if listLower in miary[1]:
            miara = "metry"


def Komenda(akcja, obiekt, miara, NaczyO):
    komenda = []
    global counter
    if akcja == "start":
        # print("Dźwig ruszył")
        komenda.append("Dźwig rusza")
    if akcja == "stop":
        # print("Dźwig się zatrzymał")
        komenda.append("Dźwig się zatrzymał")
        #p.ChangeDutyCycle(7.299999999999)
    if akcja == "lewo":
        # print("Dźwig porusza się w lewo")
        komenda.append("Dźwig porusza się w lewo")
    if akcja == "prawo":
        # print("Dźwig porusza się w prawo")
        komenda.append("Dźwig porusza się w prawo")
    if akcja == "góra" and counter == 0:
        # print("Dźwig się unosi")
        komenda.append("Dźwig się unosi")
       # p.ChangeDutyCycle(7.1)
        time.sleep(1)
        #p.ChangeDutyCycle(7.29999999999999)
        counter = 1
    if akcja == "dół" and counter == 1:
        komenda.append("Dźwig się obniża")
       # p.ChangeDutyCycle(7.5)
        time.sleep(1)
       # p.ChangeDutyCycle(7.29999999999999)
        counter = 0

    if akcja == "stopnie":
        # print("Dźwig poruszy się o ilość stopni")
        komenda.append("Dźwig porusza się o ilość stopni")
    if akcja == "obrót":
        # print("Dźwig się obraca")
        komenda.append("Dźwig się obraca")
    if obiekt == "skrzynia":
        # print ("Dźwig szuka skrzyni")
        komenda.append("Dźwig szuka skrzyni")
    if obiekt == "beczka":
        # print("Dźwig szuka beczki")
        komenda.append("Dźwig szuka beczki")
    if miara == "metry":
        komenda.append("metrów")
    if NaczyO == "o":
        komenda.append("o")
    if NaczyO == "na":
        komenda.append("na")

    for i in range(len(komenda)):
        print(komenda[i])


obiekty = [["skrzynia", "skrzynię", "skrzynkę", "skrzynka"], ["beczka", "beczkę", "beczółkę", "beczółka"]]
polecenia = [["podnieś", "unieś", "wznieś", "góra"], ["obrót", "obkręć"], ["opuść", "obniż", "dół"],
             ["zatrzymaj", "stop"], ["start", "rozpocznij"], ["lewo"], ["prawo"], ["na"], ["o"]]
miary = [["stopnie", "stopni", "st"], ["metry", "metrów"]]

# main

while True:
    obiekt = ""
    akcja = ""
    miara = ""
    NaczyO = ""
    myString = input("Podaj komende!")
    list = myString.split(" ")
    listLower = list[0].lower()
    #print(listLower)
    if len(list) == 1 and listLower == "koniec":
        print("koniec")
        #p.ChangeDutyCycle(7.29999999999)
        #p.stop()
        break
    Search(list)
    Komenda(akcja, obiekt, miara, NaczyO)